﻿using UnityEngine;
using System.Collections;

public class JumpByAddForce : MonoBehaviour
{
	Rigidbody2D rigidbody2d;
	// Use this for initialization
	void OnEnable ()
	{
		rigidbody2d = GetComponent<Rigidbody2D> ();
		StartCoroutine (Jump ());
	}
	
	// Update is called once per frame
	IEnumerator Jump ()
	{
		
		for (int i = 0; i < 10; i++) {

			rigidbody2d.AddForce (new Vector2(0, Random.Range(100,200)));
			yield return new WaitForSeconds (Random.Range(0.3f,0.6f));
		}
		gameObject.SetActive (false);
	}
}

