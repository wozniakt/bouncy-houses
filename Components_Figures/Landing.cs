﻿using UnityEngine;
using System.Collections;

public class Landing : MonoBehaviour
{
	Rigidbody2D rigidBody2d;
	float landingVelocity;
	public int pointsForSurvive, hpLoseOnDeath;
	Animator animator;
	Straightening straightening;

	// Use this for initialization
	void Start ()
	{
		rigidBody2d = GetComponent<Rigidbody2D> ();
		animator=GetComponent<Animator> ();
		straightening = GetComponent<Straightening> ();
		straightening.standStraight ();
	}

	void OnEnable(){
		straightening = GetComponent<Straightening> ();
		straightening.standStraight ();
		gameObject.layer = 8;
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if ((col.transform.CompareTag ("Ground")) && this.CompareTag ("Figure")) {
			landingVelocity = col.relativeVelocity.magnitude;
			if (landingVelocity <DataManager.instance.deathBorderLanding && this.gameObject.layer!= 9) {//6 -chyba najlepsza
				FigureRescued ();
			}
			if (landingVelocity >= DataManager.instance.deathBorderLanding) { //6
				FigureDeath ();
			} 
		}

		if ((col.transform.CompareTag ("Trampoline")) && this.CompareTag ("Figure")) {
			landingVelocity = col.relativeVelocity.magnitude;
			if (landingVelocity < 1 && this.gameObject.layer!= 9) {
				FigureRescued ();
			}
		}
	}


	void FigureRescued(){
//		if (this.name=="Pig") {
//			
//			GameObject effect = PoolManager.instance.GetPooledObject_Effect (EffectType.PinkPigExplosion);
//			effect.transform.position = new Vector2 (this.transform.position.x, this.transform.position.y - 0.25f);
//			effect.SetActive (true);
//			this.gameObject.SetActive (false);
//			gameObject.layer= 9;
//			//ManagerOfGlobalEvents.instance.TriggerOnGetPoints (pointsForSurvive*DataManager.instance.coinMultipler);
//			DataManager.instance.oPlayerData.SurvivorsFiguresCounter++;
//			SoundsManager.Instance.PlaySound (SoundType.PigBroken);
//			SoundsManager.Instance.PlaySound (SoundType.Coins);
//		}
//
//		else {
			gameObject.layer= 9;
			rigidBody2d.constraints=RigidbodyConstraints2D.FreezeRotation;
			straightening.standStraight ();
			SoundsManager.Instance.PlaySound (SoundType.Rescued);
			animator.SetBool ("HappySurvivor",true);
			//ManagerOfGlobalEvents.instance.TriggerOnGetPoints (pointsForSurvive*DataManager.instance.coinMultipler);

			DataManager.instance.oPlayerData.SurvivorsFiguresCounter++;
	//	Debug.Log	("Survive: "+DataManager.instance.oPlayerData.VictimsFiguresCounter +" "+DataManager.instance.oPlayerData.AllFiguresCounter);
					ManagerOfGlobalEvents.instance.TriggerChangePercentageSurvivorsVictims (DataManager.instance.oPlayerData.VictimsFiguresCounter / DataManager.instance.oPlayerData.AllFiguresCounter);
			//this.gameObject.SetActive (false);
//		}
	}

	public void FigureDeath(){
		SoundsManager.Instance.PlaySound (SoundType.Splash);
		GameObject effect = PoolManager.instance.GetPooledObject_Effect (EffectType.RedSplash);
		effect.transform.position = new Vector2 (this.transform.position.x, this.transform.position.y - 0.25f);
		effect.SetActive (true);
					DataManager.instance.oPlayerData.VictimsFiguresCounter++;
//		Debug.Log	("DEATH: "+DataManager.instance.oPlayerData.VictimsFiguresCounter  +" "+DataManager.instance.oPlayerData.AllFiguresCounter);
		//Debug.Log	("!!!"+DataManager.instance.oPlayerData.VictimsFiguresCounter /DataManager.instance.oPlayerData.AllFiguresCounter);
					ManagerOfGlobalEvents.instance.TriggerChangePercentageSurvivorsVictims (DataManager.instance.oPlayerData.VictimsFiguresCounter /DataManager.instance.oPlayerData.AllFiguresCounter);

//		if (DataManager.instance.oGameState==GameState.GameOn && this.name!="Pig") {
//			DataManager.instance.oPlayerData.VictimsFiguresCounter++;
//			ManagerOfGlobalEvents.instance.TriggerChangePercentageSurvivorsVictims (DataManager.instance.oPlayerData.VictimsFiguresCounter / DataManager.instance.oPlayerData.SurvivorsFiguresCounter);
//			this.gameObject.SetActive (false);
//		}

		this.gameObject.SetActive (false);
	}
	void FigureDisabled(){
		StopAllCoroutines ();
		rigidBody2d.constraints=RigidbodyConstraints2D.None;
		animator.SetBool ("HappySurvivor",false);
		GetComponentInParent<Transform>().gameObject.SetActive (false);
	}



}