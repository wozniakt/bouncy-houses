﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
	Rigidbody2D rigidBody2D;
	[HideInInspector]
	 public float velocityX, velocityY, forceJump;
	// Use this for initialization
	void Start ()
	{
		rigidBody2D = GetComponent<Rigidbody2D> ();
	
	}
	
	// Update is called once per frame
	void Update ()
	{	velocityY =rigidBody2D.velocity.y;
		rigidBody2D.velocity = new Vector2 (velocityX, velocityY);
	}


}

