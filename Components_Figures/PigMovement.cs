﻿using UnityEngine;
using System.Collections;

public class PigMovement : MonoBehaviour
{
	Animator animator;
	Rigidbody2D rigidbody2d;
	public float timeOfMoving, speed;
	// Use this for initialization
	void Start ()
	{
		rigidbody2d = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		StartCoroutine (Move (timeOfMoving, speed));
	}
	
	// Update is called once per frame
	IEnumerator Move (float timeOfMoving,float speed)
	{

		Vector3 moveDirection = gameObject.transform.position - this.transform.position; 
		rigidbody2d.isKinematic = true;
		rigidbody2d.velocity = new Vector2 (speed, 0);
		yield return new WaitForSeconds (Random.Range(timeOfMoving,timeOfMoving+2.5f));
		rigidbody2d.velocity = Vector2.zero;
		animator.SetBool ("PigFallingInit", true);
		yield return new WaitForSeconds (2);
		rigidbody2d.isKinematic = false;
		animator.SetBool ("PigFallingInit", false);
	}
}

