﻿using UnityEngine;
using System.Collections;

public class Straightening : MonoBehaviour
{
	Rigidbody2D rigidbody2d;
	// Use this for initialization
	void Start ()
	{
		rigidbody2d = GetComponent<Rigidbody2D> ();
	}
	public void standStraight(){
		StartCoroutine (Straighten ());

	}

	// Update is called once per frame
	public  IEnumerator Straighten()
	{
		for (int i = 0; i < 100; i++) {
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.Euler (0, 0, 1), 5 * Time.deltaTime);
			yield return new WaitForSeconds (0.00001f);
		}
			
		}
}


