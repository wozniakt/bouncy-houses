﻿using UnityEngine;
using System.Collections;

public class BouncingBehavior : MonoBehaviour
{
	Rigidbody2D rigidbody2d;
	// Use this for initialization

	void Start(){
		
	}
	void OnEnable ()
	{
		
		ManagerOfGlobalEvents.instance.eChangeHousesState += this.ChangeKinematicState;
		rigidbody2d = GetComponent<Rigidbody2D> ();
	}
	void OnDisable(){
		rigidbody2d.isKinematic = true;
		ManagerOfGlobalEvents.instance.eChangeHousesState -= this.ChangeKinematicState;
	}
		

	void ChangeKinematicState(HousesState housesState){
		StartCoroutine (ChangeKinematicStateCoroutine (housesState));
	}
	// Update is called once per frame
	IEnumerator ChangeKinematicStateCoroutine (HousesState housesState)
	{
		if (HousesManager.instance.oHousesState==HousesState.FinishBouncing) {
			rigidbody2d.isKinematic = true;
		} else {
			
		
		yield return new WaitForSeconds (Random.Range (0, 2));
		//rigidbody2d.isKinematic = (housesState == HousesState.StartSpawning);
		rigidbody2d.isKinematic= (HousesManager.instance.oHousesState==HousesState.StartSpawningHouses);  
		rigidbody2d.isKinematic=(HousesManager.instance.oHousesState!=HousesState.BouncingHouses);
		rigidbody2d.isKinematic= (HousesManager.instance.oHousesState==HousesState.FinishBouncing);
		rigidbody2d.gravityScale = Random.Range (0.5f, 5f);
		}
	}
}

