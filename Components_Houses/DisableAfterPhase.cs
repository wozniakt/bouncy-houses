﻿using UnityEngine;
using System.Collections;

public class DisableAfterPhase : MonoBehaviour
{
	public HousesState housePhaseWhenDisable;
	// Use this for initialization
	void OnEnable(){
		ManagerOfGlobalEvents.instance.eChangeHousesState += this.disableHouse;
	}

	void OnDisable(){
		ManagerOfGlobalEvents.instance.eChangeHousesState -= this.disableHouse;
	}

	void disableHouse(HousesState houseState){
		if (houseState==housePhaseWhenDisable) {
			this.gameObject.SetActive (false);
			GameObject effect = PoolManager.instance.GetPooledObject_Effect (EffectType.Pow);
			effect.transform.position = this.transform.position;
			effect.SetActive (true);
		}

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

