﻿using UnityEngine;
using System.Collections;

public class KillFigures : MonoBehaviour
{

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag("Figure")) {
			other.GetComponent<Landing> ().FigureDeath ();
		}
	}
}

