﻿using UnityEngine;
using System.Collections;

public class RestartGameObjectPosition : MonoBehaviour
{
	Vector2 startPosition;
	// Use this for initialization

	void Start(){
		startPosition = this.transform.localPosition;

	}

	void OnEnable ()
	{
		this.transform.localPosition = startPosition;
	
		//this.gameObject.GetComponent <Rigidbody2D> ().isKinematic=false;
	}
	
	// Update is called once per frame

}

