﻿using UnityEngine;
using System.Collections;

public class Ctrl_Jump_Figure : MonoBehaviour
{
	Rigidbody2D rigidBody2D;
	public float forceJump;
	// Use this for initialization
	void Start ()
	{
		
		//rigidBody2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void OnEnable ()
	{
		
		rigidBody2D = GetComponent<Rigidbody2D> ();
		JumpFromRoof (forceJump);
	}

	public void JumpFromRoof(float forceJump){
		rigidBody2D.AddForce (new Vector2 (forceJump-Random.Range(1,100), 0));
	}
}

