﻿using UnityEngine;
using System.Collections;
using CnControls;


[RequireComponent(typeof(Movement))]
public class Ctrl_Move_Trampoline : MonoBehaviour
{
	Animator animator;
	public float VelocityX, VelocityY;
	float horizontalInput;
	Movement movement;
	Rigidbody2D rigidbody2d;
	bool canMove;
	// Use this for initialization
	void Start ()
	{
		animator = GetComponent<Animator> ();
		//canMove = false;
		rigidbody2d = GetComponent<Rigidbody2D> ();
		//rigidbody2d.isKinematic =! canMove;
	
		movement = GetComponent<Movement> ();
		//StartCoroutine (startMove ());

	}
	void OnEnable(){
	//	canMove = false;
	//	rigidbody2d = GetComponent<Rigidbody2D> ();
	//	rigidbody2d.isKinematic =! canMove;
	}

	IEnumerator startMove(){
	//	Debug.Log (rigidbody2d.isKinematic +"canMove" + canMove);
		yield return new WaitForSeconds (0.2f);
		canMove = true;
	//	Debug.Log (rigidbody2d.isKinematic +"canMove" + canMove);
	}
	// Update is called once per frame
	void Update ()
	{
		//rigidbody2d.isKinematic =! canMove;
		//if (canMove) {
			
		horizontalInput = CnInputManager.GetAxis ("Horizontal");
		movement.velocityX = horizontalInput*VelocityX;
		if (horizontalInput > 0) {
			movement.velocityX = 1*VelocityX;
			animator.SetBool ("isMoving", true);
		}
		if (horizontalInput < 0) {
			movement.velocityX = -1*VelocityX;
			animator.SetBool ("isMoving", true);

		} if (horizontalInput == 0) {
			movement.velocityX = 0;
			animator.SetBool ("isMoving", false);
		

		}
		//}
	}
}

