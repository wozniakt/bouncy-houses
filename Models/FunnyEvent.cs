﻿using System;
using UnityEngine;


	[System.Serializable]
	public class FunnyEvent
	{



		[SerializeField]
		int number;
		public int Number
		{
			get
			{return number;}
			set{

				number= value;

			}
		}
	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked
	{
		get
		{return isUnlocked;}
		set{

			isUnlocked= value;

		}
	}
		[SerializeField]
		string description;
		public string Description
		{
			get
			{return description;}
			set{

				description= value;

			}
		}


		public FunnyEvent ()
		{
		}
	}


