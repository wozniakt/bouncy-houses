﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Stage
{


	[SerializeField]
	int pointsToUnlock;
	public int PointsToUnlock
	{
		get
		{return pointsToUnlock;}
		set{

			pointsToUnlock= value;

		}
	}


	[SerializeField]
	int number;
	public int Number
	{
		get
		{return number;}
		set{

			number= value;

		}
	}

	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked {
		get
		{ return isUnlocked; }
		set {

			isUnlocked = value;

		}
	}

	[SerializeField]
	bool comingSoon;
	public bool ComingSoon {
		get
		{ return comingSoon; }
		set {

			comingSoon = value;

		}
	}
}