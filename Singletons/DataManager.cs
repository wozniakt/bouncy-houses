﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour {

	public int pointsMultipler;
	public static string PREFAB_PATH = "Prefabs/";
	public  PlayerData oPlayerData;
	public static DataManager instance;
	public GameState oGameState;
	public float deathBorderLanding;
	public float intervalFiguresSpawner;
	public int coinMultipler;
	public int eventsCost;
	public float bouncingTimerStart;
	public float failPercentage, easyMode, mediumMode,hardMode;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}
	// Use this for initialization
	void Start () {
		ManagerOfGlobalEvents.instance.OnChangeGameState += this.ChangeGameState;
		ManagerOfGlobalEvents.instance.eGetPoints += this.UpdatePoints;
		//ManagerOfGlobalEvents.instance.OnLifeLose += this.UpdateHp;
		oPlayerData.CoinsCount = PlayerPrefs.GetInt ("CoinsCount");
		oPlayerData.HighscorePointsCount = PlayerPrefs.GetInt ("HighScore");
		oPlayerData.MaxUnlockedFunnyEventNo = Mathf.RoundToInt(oPlayerData.HighscorePointsCount / 10)+2;
		oPlayerData.PointsCount = 0;
		oPlayerData.HpCount = 3;
		oPlayerData.ComboCount=1;
		oGameState = new GameState ();

	}

	public void  UpdatePoints(int points){
		if (oGameState == GameState.GameOn) {
			oPlayerData.PointsCount = oPlayerData.PointsCount +(points*1*pointsMultipler);

			oPlayerData.HighscorePointsCount = Mathf.Max (oPlayerData.HighscorePointsCount, oPlayerData.PointsCount);
			ManagerOfGlobalEvents.instance.TriggerDataChange ();
		}
		if (oGameState != GameState.GameOn) {
			oPlayerData.PointsCount =0;
			ManagerOfGlobalEvents.instance.TriggerDataChange ();
		}
	}

//	public void  UpdatePoints(int points){
//		if (oGameState==GameState.GameOn) {
//			
//	
//		if ((DataManager.instance.oPlayerData.ComboCount >1)) {
//				oPlayerData.CoinsCount = oPlayerData.CoinsCount+(points*Mathf.RoundToInt(oPlayerData.ComboCount));
//				oPlayerData.PointsCount = oPlayerData.PointsCount +(points*Mathf.RoundToInt(oPlayerData.ComboCount));
//		} else {
//			oPlayerData.CoinsCount = oPlayerData.CoinsCount+(points*1);
//			oPlayerData.PointsCount = oPlayerData.PointsCount +(points*1);
//		}		
//		oPlayerData.HighscorePointsCount = Mathf.Max (oPlayerData.HighscorePointsCount, oPlayerData.PointsCount);
//			oPlayerData.ComboCount2 = oPlayerData.ComboCount2 +1;
//			oPlayerData.ComboCount = Mathf.RoundToInt (oPlayerData.ComboCount2/5);
//
//		ManagerOfGlobalEvents.instance.TriggerDataChange ();
//		}	
//	}

	public void  UpdateHp(int hp){
		
		oPlayerData.HpCount = oPlayerData.HpCount-hp;
		if (hp>=0) {
			oPlayerData.ComboCount = 1; //????????????????
			oPlayerData.ComboCount2=1;
		}
		if (oPlayerData.HpCount<=0) {
			oPlayerData.ComboCount = 1;
			oPlayerData.ComboCount2 = 1;
			ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameLost);
		}
		ManagerOfGlobalEvents.instance.TriggerDataChange ();
	}

	public void  ChangeGameState (GameState gameState)
	{

		oGameState = gameState;
		//DataManager.instance.oGameState = gameState;
		switch (oGameState) {
		case GameState.GameOn:
			//HousesManager.instance.bouncingTimer = 10;
			oPlayerData.PointsCount=0;
			HousesManager.instance.oHousesState = HousesState.StartSpawningHouses;
			oPlayerData.CurrentHousesNumber = 0;
			ManagerOfGlobalEvents.instance.TriggerChangeHousesState (HousesState.StartSpawningHouses);
			Time.timeScale = 1;
			break;
		case GameState.Menu:
			
			//ManagerOfGlobalEvents.instance.TriggerNotEnoughMoney("You can change Game Mode. Only for " + eventsCost.ToString() + " coins");
			Time.timeScale = 1;
			ManagerOfGlobalEvents.instance.TriggerDataChange ();
			break;
		case GameState.GamePause:
			Time.timeScale = 0;
			break;

	

		case GameState.GameLost:
			SoundsManager.Instance.PlaySound (SoundType.Applause);
			Time.timeScale = 0;
			PlayerPrefs.SetInt ("HighScore",oPlayerData.HighscorePointsCount);
			break;
		}
	}

	public void ReStartGame(Stage oStage, float levelEasyMediumHard, int pointsMultipler){
		UIStagesManager.instance.UpdateStages ();

		Debug.Log("oStage.IsUnlocked: "+oStage.IsUnlocked +" oStage.ComingSoon: "+oStage.ComingSoon+" number: " +oStage.Number);
		if (oStage.IsUnlocked) {
			SoundsManager.Instance.PlaySound (SoundType.Button);
			DataManager.instance.failPercentage =levelEasyMediumHard;

			DataManager.instance.pointsMultipler = pointsMultipler;
			StartCoroutine (ReStartGameCoroutine ());
			UImanager.instance.StopAllCoroutines();

		} else {
			SoundsManager.Instance.PlaySound (SoundType.NotEnoughMoney);
			UImanager.instance.PanelBackground_Animator.SetTrigger("UnsuccesfulBought");
			//Img_RandomEvent_Animator.SetTrigger("UnsuccesfulBought");

		}
	}
		
	public IEnumerator ReStartGameCoroutine(){

		AsyncOperation async = SceneManager.LoadSceneAsync (0);
		async.allowSceneActivation = true;
		yield return async;
		ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameOn);
		oPlayerData.ComboCount = 0;
		oPlayerData.PointsCount = 0;
		oPlayerData.HpCount = 3;
		UImanager.instance.createScene ();
		UImanager.instance.UpdateHudTexts ();
		UImanager.instance.UpdateHpText ();
		UImanager.instance.UpdateComboText ();
		Time.timeScale = 1;
		//FunnyEventsManager.instance.doEvent (oPlayerData.CurrentFunnyEvent.Number);
		ManagerOfGlobalEvents.instance.TriggerDataChange ();
	}

	public void PauseGame(){
		UImanager.instance.ButtonStopSpawnInGame.gameObject.SetActive (false);
		Time.timeScale = 0;
		UImanager.instance. PanelPause.SetActive (true);
		SoundsManager.Instance.PlaySound (SoundType.Button);
		//ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GamePause);
		PlayerPrefs.SetInt ("HighScore",oPlayerData.HighscorePointsCount);
	}

	public void ResumeGame(){
		UImanager.instance.ButtonStopSpawnInGame.gameObject.SetActive (true);
		Time.timeScale = 1;
		UImanager.instance. PanelPause.SetActive (false);
		SoundsManager.Instance.PlaySound (SoundType.Button);
		//ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameOn);
	}

	public void ExitGame(){
		SoundsManager.Instance.PlaySound (SoundType.Button);
		//PlayerPrefs.SetInt ("CoinsCount",oPlayerData.CoinsCount);
		PlayerPrefs.SetInt ("HighScore",oPlayerData.HighscorePointsCount);
		Application.Quit ();
	}

	public void BackToMenu(){
		SoundsManager.Instance.PlaySound (SoundType.Button);
		PlayerPrefs.SetInt ("CoinsCount",oPlayerData.CoinsCount);
		PlayerPrefs.SetInt ("HighScore",oPlayerData.HighscorePointsCount);
		oPlayerData.MaxUnlockedFunnyEventNo = Mathf.RoundToInt(oPlayerData.HighscorePointsCount / 10)+3;
		if (DataManager.instance.oGameState!=GameState.Menu) {
			ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.Menu);
		}
		UImanager.instance.ButtonQuick_Restart.StopAllCoroutines ();
		UImanager.instance.StopCoroutine("finishGame" );
	}
		

}
