﻿using UnityEngine;
using System.Collections;

public enum GameState {Menu, GameOn, GamePause, GameLost}

public enum HousesState {StopAll,StartSpawningHouses, BouncingHouses,preFinishBouncing, FinishBouncing, SpawningFigures, afterFinishSpawningFigures}


public enum FigureType {Human, Human1,Human2, Human3}

public enum EffectType {Empty, RedSplash, CanvasCloud, HappyJump, PinkPigExplosion, Coins, Pow}

public enum SoundType
{
	Splash, Coins, Button, Reflect, Jump, Pig, Rescued, PigBroken,Applause, Meow,NotEnoughMoney, Pop, Dissapear
}


