﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class HousesManager : MonoBehaviour
{
	public HousesState oHousesState;
	public static HousesManager instance;
	public float bouncingTimer, startBouncingTimer;
	float failPercentage;
	DataManager dataManager;
	UImanager uiManager;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}


	// Use this for initialization
	void Start ()
	{
		startBouncingTimer = bouncingTimer;
		uiManager = UImanager.instance;
		dataManager = DataManager.instance;
		oHousesState = new HousesState ();
		ManagerOfGlobalEvents.instance.eChangeHousesState += this.ChangeHousesStateInit;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (oHousesState==HousesState.BouncingHouses) {
			bouncingTimer -= Time.deltaTime;
			//UImanager.instance.bouncingTimer.text = (Mathf.Round( bouncingTimer)).ToString ();
		}
	}
	void FixedUpdate(){

		uiManager.pointerClock.transform.localRotation = Quaternion.Euler(0f, 0f,360*bouncingTimer/startBouncingTimer);
	}

	void ChangeHousesStateInit(HousesState housesState){
		StartCoroutine(ChangeHousesState (housesState));
	}


	public IEnumerator  ChangeHousesState (HousesState housesState)
	{
		oHousesState = housesState;

		if (DataManager.instance.oGameState==GameState.GameOn) {
			
		
		switch (oHousesState) {


		case HousesState.StartSpawningHouses:
			//uiManager.pointerClock.transform.localRotation = Quaternion.Euler(0f, 0f,360*bouncingTimer/startBouncingTimer);
			bouncingTimer = 0;
			UImanager.instance.UpdatePercentageImage (0);
			DataManager.instance.oPlayerData.AllFiguresCounter = 0;
			DataManager.instance.oPlayerData.SurvivorsFiguresCounter = 0;
			DataManager.instance.oPlayerData.VictimsFiguresCounter = 0;
			SpawnerHouses.instance.SpawnHousesWaves ();
			break;
		case HousesState.BouncingHouses:
			bouncingTimer = DataManager.instance.bouncingTimerStart;
			startBouncingTimer = bouncingTimer;
			break;


		case HousesState.FinishBouncing:
			//ManagerOfGlobalEvents.instance.TriggerChangeHousesState (HousesState.afterFinishSpawningFigures);
			break;
		case HousesState.afterFinishSpawningFigures:
				SoundsManager.Instance.PlaySound (SoundType.Dissapear);
			yield return new WaitForSeconds (1.0f);
			if ((DataManager.instance.oPlayerData.VictimsFiguresCounter/DataManager.instance.oPlayerData.AllFiguresCounter)>=dataManager.failPercentage) {
					SoundsManager.Instance.PlaySound (SoundType.Splash);
					ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameLost);

			} else {
				yield return new WaitForSeconds (1.0f);

				ManagerOfGlobalEvents.instance.TriggerChangeHousesState (HousesState.StartSpawningHouses);
				ManagerOfGlobalEvents.instance.TriggerGetPoints (1);
					SoundsManager.Instance.PlaySound (SoundType.Coins);
			}
		
			//SpawnerHouses.instance.SpawnHousesWaves ();
			break;
		
		}
		}}

}

