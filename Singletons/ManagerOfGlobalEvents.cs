﻿using UnityEngine;
using System.Collections;

public class ManagerOfGlobalEvents : MonoBehaviour
{

	public static ManagerOfGlobalEvents instance;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}

	public delegate void ChangeHousesState(HousesState housesState);
	public event ChangeHousesState eChangeHousesState;

	public void TriggerChangeHousesState(HousesState housesState){
		if (eChangeHousesState!=null) {
			eChangeHousesState (housesState); 
		}
	}



	public delegate void GetPoints(int points);
	public event GetPoints eGetPoints;

	public void TriggerGetPoints (int points){
		if (eGetPoints!=null) {
			eGetPoints (points); 
		}
	}



	public delegate void ChangeGameState(GameState gameState);
	public event ChangeGameState OnChangeGameState;

	public void TriggerOnChangeGameState(GameState gameState){
		if (OnChangeGameState!=null) {
			OnChangeGameState (gameState); 
		}
	}

	public delegate void AchievementUnlock(FunnyEvent  funnyEvent);
	public event AchievementUnlock eAchievementUnlock;

	public void TriggerAchievementUnlock(FunnyEvent funnyEvent){
		if (eAchievementUnlock!=null) {
			eAchievementUnlock (funnyEvent); 
		}
	}

	public delegate void ChangeEventImage(string EventImageName);
	public event ChangeEventImage eChangeEventImage;

	public void TriggerChangeEventImage(string EventImageName){
		if (eChangeEventImage!=null) {
			eChangeEventImage (EventImageName); 
		}
	}

	public delegate void NotEnoughMoney(string TextToShow);
	public event NotEnoughMoney eNotEnoughMoney;

	public void TriggerNotEnoughMoney(string TextToShow){
		if (eNotEnoughMoney!=null) {
			eNotEnoughMoney (TextToShow); 
		}
	}

	public delegate void DataChange();
	public event DataChange eDataChange;

	public void TriggerDataChange(){
		if (eDataChange!=null) {
			eDataChange (); 
		}
	}



	public delegate void ChangePercentageSurvivorsVictims(float percentage);
	public event ChangePercentageSurvivorsVictims eChangePercentageSurvivorsVictims;

	public void TriggerChangePercentageSurvivorsVictims(float percentage){
		if (eChangePercentageSurvivorsVictims!=null) {
			eChangePercentageSurvivorsVictims ( percentage);// 
		}
	}



	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

