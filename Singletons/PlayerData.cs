﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerData 
{


	[SerializeField]
	float allFiguresCounter;
	public float AllFiguresCounter
	{
		get
		{return allFiguresCounter;}
		set{

			allFiguresCounter=value;
//			if (allFiguresCounter==0) {
//				allFiguresCounter = 1;
//			}
		}
	}

	[SerializeField]
	float survivorsFiguresCounter;
	public float SurvivorsFiguresCounter
	{
		get
		{return survivorsFiguresCounter;}
		set{

			survivorsFiguresCounter= value;
			//AllFiguresCounter = allFiguresCounter;
		}
	}


	[SerializeField]
	float victimsFiguresCounter;
	public float VictimsFiguresCounter
	{
		get
		{return victimsFiguresCounter;}
		set{

			victimsFiguresCounter= value;

		}
	}
		

	int maxFunnyEventToUnlock,newFunnyEventToUnlockNo;
	FunnyEvent newFunnyEventToUnlock;

	[SerializeField]
	float comboCount;
	public float ComboCount
	{
		get
		{return comboCount;}
		set{

			comboCount= value;

		}
	}


	[SerializeField]
	Stage currentStage;
	public Stage CurrentStage
	{
		get
		{return currentStage;}
		set{


			currentStage = value;
		}
	}

	[SerializeField]
	int currentHousesNumber;
	public int CurrentHousesNumber
	{
		get
		{return currentHousesNumber;}
		set{

			currentHousesNumber= value;

		}
	}


	[SerializeField]
	int maxHousesNumber;
	public int MaxHousesNumber
	{
		get
		{return maxHousesNumber;}
		set{

			maxHousesNumber= value;

		}
	}




	[SerializeField]
	float comboCount2;
	public float ComboCount2
	{
		get
		{return comboCount2;}
		set{

			comboCount2= value;

		}
	}
		

	[SerializeField]
	int pointsCount;
	public int PointsCount
	{
		get
		{return pointsCount;}
		set{

			pointsCount= value;

		}
	}

//	[SerializeField]
//	List<FunnyEvent> funnyEventsList;
//	public List<FunnyEvent> FunnyEventsList{
//		get
//		{return funnyEventsList;}
//		set{
//
//			funnyEventsList = value;  //new List<Creature> (DataManager.instance.heroesList);
//			//		coinsCount = DataManager.instance.coins;
//		}
//	}
	[SerializeField]
	List<House> housesList;
	public List<House> HousesList{
		get
		{return housesList;}
		set{
			housesList = value;  //new List<Creature> (DataManager.instance.heroesList);
		}
	}



	[SerializeField]
	FunnyEvent currentFunnyEvent;
	public FunnyEvent CurrentFunnyEvent
	{
		get
		{return currentFunnyEvent;}
		set{
			currentFunnyEvent= value;
		}
	}
	[SerializeField]
	int maxUnlockedFunnyEventNo;
	public int MaxUnlockedFunnyEventNo
	{
		get
		{return maxUnlockedFunnyEventNo;}
		set{
			if (value==0) {
				value = 1;
			}
			maxUnlockedFunnyEventNo= value;

		}
	}
	[SerializeField]
	int highscorePointsCount;
	public int HighscorePointsCount
	{
		get
		{return highscorePointsCount;}
		set{
			if (highscorePointsCount<value) {
			highscorePointsCount= value;

			
			}

		}
	}


	[SerializeField]
	int coinsCount;
	public int CoinsCount
	{
		get
		{return coinsCount;}
		set{
			if (value>=0) {
				coinsCount= value;
			} else {
				coinsCount = 0;		
			}	

			PlayerPrefs.SetInt ("CoinsCount", CoinsCount);
		}
	}

	[SerializeField]
	int hpCount;
	public int HpCount
	{
		get
		{return hpCount;}
		set{
			hpCount= value;

		}
	}

}