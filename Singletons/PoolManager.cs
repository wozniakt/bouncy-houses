﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

	public static string POOLED_PREFAB_PATH = "Prefabs/";
	public static PoolManager instance;
	public GameObject FiguresPool, EffectsPool,HousesPool;
	public bool WillGrow = true;


	List<GameObject> pooledFigures;
	List<GameObject> pooledEffects;
	List<GameObject> pooledHouses;
	void Awake()
	{  
		instance = this;
		pooledFigures = new List<GameObject>();
		pooledEffects = new List<GameObject>();
		pooledHouses = new List<GameObject>();
		createFigures(10,0);
		//createFigures(6,1);
		createFigures (10, 2);
		createFigures(10,3);
		//createFigures(6,4);
		createFigures(10,5);
		//createFigures(6,6);
//		createFigures(2,7);
//		createFigures(2,8);
		createFigures(4,100);
		createHouse(10,1);
		createEffects (3, EffectType.RedSplash);
		createEffects (3, EffectType.PinkPigExplosion);
		createEffects (3, EffectType.Pow);
		// tu mzoesz zrobic dynamciznie na podstawie slected character bullets
	}


	void createHouse(int count, int houseNumber)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Houses_Prefabs/House"+houseNumber.ToString()));

			obj.SetActive(false);
			obj.name = "House"+houseNumber.ToString();
			pooledHouses.Add(obj);
			obj.transform.SetParent(HousesPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_House(int houseNumber)
	{

		foreach (GameObject item in pooledHouses)
		{ 
			if (item.name == "House"+houseNumber.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} 
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Houses_Prefabs/House"+ houseNumber));
			obj.name="House"+houseNumber.ToString();
			pooledHouses.Add(obj);
			obj.transform.SetParent(HousesPool.transform);
			return obj;
		}
		return null;
	}


	void createFigures(int count, int randomFigureNumber)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Figures_Prefabs/Figure"+randomFigureNumber.ToString()));

			obj.SetActive(false);
			obj.name = "Figure"+randomFigureNumber.ToString();
			pooledFigures.Add(obj);
			obj.transform.SetParent(FiguresPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_Figure(int randomFigureNumber)
	{
		foreach (GameObject item in pooledFigures)
		{
			if (item.name == "Figure"+randomFigureNumber.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} 
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Figures_Prefabs/Figure"+ randomFigureNumber));
			obj.name="Figure"+randomFigureNumber.ToString();
			pooledFigures.Add(obj);
			obj.transform.SetParent(FiguresPool.transform);
			return obj;
		}
		return null;
	}

	void createEffects(int count, EffectType effectType)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Effects_Prefabs/"+effectType.ToString()));
			obj.SetActive(false);
			obj.name = effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_Effect(EffectType effectType)
	{
		foreach (GameObject item in pooledEffects)
		{
			if (item.name == effectType.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Effects_prefabs/"+ effectType.ToString()));
			obj.name=effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
			return obj;
		}
		return null;
	}



}




