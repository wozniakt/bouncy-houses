﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerHouses : MonoBehaviour
{
	public float initInterval,betweenHousesInterval;
	public int housesCount;
	public static SpawnerHouses instance;
	public EffectType effectType;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
//		DontDestroyOnLoad (instance.gameObject);
		//SpawnHousesWaves();
	}




	public int houseNumber;
	public void SpawnHousesWaves(){
		StartCoroutine (SpawnSingleWaveHouses (initInterval,betweenHousesInterval,housesCount,effectType));
	}
	// Update is called once per frame
	IEnumerator SpawnSingleWaveHouses (float initInterval, float betweenHousesInterval, int housesCount, EffectType effectType)
	{
			yield return new WaitForSeconds (initInterval);
		housesCount = Random.Range (4, housesCount + 1);
		for (int i = 0; i < housesCount; i++) {
			
			houseNumber = Mathf.RoundToInt( DataManager.instance.oPlayerData.HighscorePointsCount / 10);
			GameObject house=PoolManager.instance.GetPooledObject_House (Random.Range(0,houseNumber+1));
			house.SetActive (false);
			house.transform.position =new Vector2(Random.Range(-8.0f,8.0f),Random.Range(6.0f,8.0f));
			GameObject effect = PoolManager.instance.GetPooledObject_Effect (effectType);
			effect.transform.position = house.transform.position;
			effect.SetActive (true);
			yield return new WaitForSeconds (0.3f);
			SoundsManager.Instance.PlaySound (SoundType.Pop);
			house.SetActive (true);
			DataManager.instance.oPlayerData.CurrentHousesNumber++;
			yield return new WaitForSeconds (betweenHousesInterval);
		}
			if (DataManager.instance.oPlayerData.CurrentHousesNumber>=DataManager.instance.oPlayerData.MaxHousesNumber) {
			yield return new WaitForSeconds (1);
			StopAllCoroutines ();
				ManagerOfGlobalEvents.instance.TriggerChangeHousesState (HousesState.BouncingHouses);
		
			}
		StopAllCoroutines ();
			}


	void Update(){
		
		if (HousesManager.instance.oHousesState == HousesState.BouncingHouses && HousesManager.instance.bouncingTimer<=0.0f) {
			ManagerOfGlobalEvents.instance.TriggerChangeHousesState (HousesState.FinishBouncing);
		}
	}

	public void StopSpawnHouses(){
	
		if ( HousesManager.instance.oHousesState == HousesState.BouncingHouses && HousesManager.instance.bouncingTimer>0.0f && Time.timeScale>0) {
			ManagerOfGlobalEvents.instance.TriggerChangeHousesState (HousesState.FinishBouncing);
		}
	}

}

