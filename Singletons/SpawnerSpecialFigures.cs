﻿using UnityEngine;
using System.Collections;

public class SpawnerSpecialFigures : MonoBehaviour
{
	public float intervalSpawn;
	Vector2 placeHolder_Figures;
	int DownBound, UpperBound;
	int randomFigureNumber;
	public int maxFigureNumber;
	public string placeHolderName;
	// Use this for initialization
	void Start ()
	{

		//intervalSpawn = DataManager.instance.intervalFiguresSpawner;
		placeHolder_Figures= GameObject.Find (placeHolderName).transform.position;
		StartCoroutine (spawnFigure (intervalSpawn));
	}


	IEnumerator spawnFigure(float interval){
		yield return new WaitForSeconds (interval/2);
		if (DataManager.instance.oPlayerData.PointsCount>2) {
			
		
		GameObject figure=PoolManager.instance.GetPooledObject_Figure (100);
		figure.SetActive (true);
		figure.transform.position=placeHolder_Figures;
		SoundsManager.Instance.PlaySound (SoundType.Pig);
		}
		yield return new WaitForSeconds (interval / 2);

		if (DataManager.instance.oGameState==GameState.GameOn) {
			//intervalSpawn = DataManager.instance.intervalFiguresSpawner/2;
			StartCoroutine (spawnFigure (intervalSpawn));


		}

	}
}


