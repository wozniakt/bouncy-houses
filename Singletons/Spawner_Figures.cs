﻿   	using UnityEngine;
using System.Collections;

public class Spawner_Figures : MonoBehaviour
{
	public float intervalSpawn;

	public int maxFigureNumber;
	public int FiguresAmount,FigureNumber;
	// Use this for initialization
	void OnEnable ()
	{
		ManagerOfGlobalEvents.instance.eChangeHousesState += this.startSpawnFiguresAfterBouncing;
		//StartCoroutine (spawnFigure (intervalSpawn,FiguresAmount,FigureNumber));
	}
	
	void OnDisable(){
		ManagerOfGlobalEvents.instance.eChangeHousesState -= this.startSpawnFiguresAfterBouncing;
	}

	IEnumerator spawnFigure(float interval, int FiguresAmount, int FigureNumber){
		//if (DataManager.instance.oGameState==GameState.GameOn){
		if (HousesManager.instance.oHousesState==HousesState.FinishBouncing) {

			for (int i = 0; i < FiguresAmount; i++) {
				GameObject figure=PoolManager.instance.GetPooledObject_Figure (FigureNumber);
				figure.transform.position=this.transform.position;
				figure.SetActive (true);
				SoundsManager.Instance.PlaySound (SoundType.Jump);
				yield return new WaitForSeconds (interval);
			}
			yield return new WaitForSeconds (1f);
			ManagerOfGlobalEvents.instance.TriggerChangeHousesState (HousesState.afterFinishSpawningFigures);
		}
	}


	public void startSpawnFiguresAfterBouncing(HousesState oHousesState){
		if (oHousesState == HousesState.FinishBouncing ) {
			StartCoroutine (spawnFigure (intervalSpawn,FiguresAmount,FigureNumber));
		}
	}
}

