﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIStagesManager : MonoBehaviour
{
	public GameObject StageScrollRect;
	public List<GameObject> StagesList;
	public static UIStagesManager instance;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}

	// Use this for initialization
	void Start ()
	{
		ManagerOfGlobalEvents.instance.eDataChange += this.UpdateStages;
		UpdateStages ();
		//StagesList = new List<Stage> ();
	}
	
	// Update is called once per frame
	public void UpdateStages()
	{




		DataManager.instance.oPlayerData.CurrentStage= UIStagesManager.instance.StagesList[UIStagesManager.instance.StageScrollRect.GetComponent<ScrollSnapRect>()._currentPage].GetComponent<StageInit>().oStage;
		foreach (GameObject goStage in StagesList) {
			Stage oStage = goStage.GetComponent<StageInit> ().oStage;
			if (oStage.IsUnlocked==false) {
				goStage.GetComponent<Image> ().color = Color.gray;
				goStage.GetComponentInChildren<Text>().text="Unlock at "+(20*oStage.Number).ToString()+" points";
			}
			if (oStage.ComingSoon==true) {
				goStage.GetComponent<Image> ().color = Color.red;
				goStage.GetComponentInChildren<Text>().text="COMING SOON...";
			}
			if (oStage.ComingSoon==false && oStage.IsUnlocked==true) {
				goStage.GetComponent<Image> ().color = Color.gray;
				goStage.GetComponentInChildren<Text>().text="";
			}
		}
	}
}

