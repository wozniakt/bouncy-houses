﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class UImanager : MonoBehaviour
{
	ManagerOfGlobalEvents managerOfGlobEvents;
	public Text bouncingTimer;
	public GameObject pointerClock;
	public GameObject timerPlaceHolder1, timerPlaceHolder2, timer;
	public GameObject imgVictimsMarker;
	public Image imgVictimsPercent;
	public static string PREFAB_PATH = "Prefabs/";
	public Text coinsText, pointsText,highscoreText,FunnyEventDescriptionText;
	public Text achievementText;
	public Image imgPercentCounterVictims;
	public GameObject GameBackground, GameOverTextAsImage;
	public  Image imgHp, imgAchievement,img_RandomEvent, ImageEvent_Visualization;
	public GameObject PanelPause, PanelMenu, PanelBackground;
	public Button ButtonMenu_SoundOff,ButtonMenu_MusicOff;
	public Button ButtonMenu_ReStart_Easy,ButtonMenu_ReStart_Hard,ButtonMenu_ReStart_Medium,  ButtonPause_Resume,ButtonPause_Pause, ButtonPause_RestartGame, ButtonPause_BackToMenuAfterLost,ButtonPause_BackToMenu;
	public Button ButtonGameWon_ReStart,ButtonStopSpawnInGame;
	public Button ButtonMenu_RandomEvent, ButtonQuick_Restart;
	public Button ButtonMenu_WatchAdd, ButtonMenu_Exit;
	//public GameObject Menu_Title;
	public GameObject Panel_GameLost;
	public static UImanager instance;
	public Animator Img_RandomEvent_Animator,PanelBackground_Animator;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
	}
		
	void Start ()
	{	managerOfGlobEvents = ManagerOfGlobalEvents.instance;
		
		managerOfGlobEvents.eChangeEventImage += this.changeEventImage;
		managerOfGlobEvents.eNotEnoughMoney += this.ShowTextInComicCloud;
		managerOfGlobEvents.eDataChange += this.UpdateEventDescript;
		managerOfGlobEvents.eDataChange += this.UpdateHudTexts;
		managerOfGlobEvents.OnChangeGameState += this.UIChangeGameState;
		managerOfGlobEvents.eDataChange += this.UpdateComboText;
		managerOfGlobEvents.eDataChange += this.UpdateHpText;
		managerOfGlobEvents.eDataChange += this.UpdateComboText;
		managerOfGlobEvents.eChangePercentageSurvivorsVictims += this.UpdatePercentageImage;
		//managerOfGlobEvents.eAchievementUnlock += this.ShowUnlockedAchievement;
		createScene ();

		AddListenersToButtons ();
		Img_RandomEvent_Animator = img_RandomEvent.GetComponent<Animator> ();
		PanelBackground_Animator = PanelBackground.GetComponent<Animator> ();
		managerOfGlobEvents.TriggerOnChangeGameState (GameState.Menu);
		managerOfGlobEvents.TriggerDataChange ();
	}

	public void createScene(){
		// aaahhh co sie tu dzieje xD?
		// scaszuj se te placeholdery
		float imgVictimsWidth=imgVictimsPercent.rectTransform.rect.width;

		imgVictimsMarker.transform.localPosition = new Vector2 (DataManager.instance.failPercentage * (imgVictimsWidth)-5 , 0);
		GameObject ground = (GameObject)Instantiate(Resources.Load(PREFAB_PATH +"Other_Prefabs/"+"Ground"));
		ground.transform.SetParent ( GameObject.Find ("PlaceHolder_Ground").transform);
		ground.transform.position= GameObject.Find ("PlaceHolder_Ground").transform.position;


		//GameObject timer = (GameObject)Instantiate(Resources.Load(PREFAB_PATH +"UI_Prefabs/"+"Timer"));
		timer.transform.SetParent ( GameObject.Find ("Timer_UI_Placeholder"+DataManager.instance.oPlayerData.CurrentStage.Number).transform);
		timer.transform.position= GameObject.Find ("Timer_UI_Placeholder"+DataManager.instance.oPlayerData.CurrentStage.Number).transform.position;

		Sprite newSprite =(Resources.Load<Sprite> ((PREFAB_PATH +"Sprites/Backgrounds/"  + DataManager.instance.oPlayerData.CurrentStage.Number)));
		GameBackground.GetComponent<SpriteRenderer> ().sprite = newSprite;

	}

	public void ChangeBackground(string spriteName){

		Sprite newSprite =(Resources.Load<Sprite> ("Prefabs/Events_Prefabs/Event_"  + spriteName));
		GameBackground.GetComponent<SpriteRenderer> ().sprite = newSprite;
	}

	public void UpdatePercentageImage(float percentage){
		imgPercentCounterVictims.fillAmount =percentage;
	}

	public void  UpdateHudTexts(){
		// po co ten parametr skoro go nie wykorzstujesz??
		// powinienes wlasnie update zrobic na podstawie parametru.
		// Osobne : UpdateCoinsText i UpdatePointsText, chyba ze wiesz, a to jest "póki co"
			coinsText.text = "COINS: "+DataManager.instance.oPlayerData.CoinsCount;
			pointsText.text = "POINTS: " + DataManager.instance.oPlayerData.PointsCount;
			highscoreText.text="HIGHSCORE: "+DataManager.instance.oPlayerData.HighscorePointsCount;
	}

	public void  ShowUnlockedAchievement(FunnyEvent funnyEvent){
		imgAchievement.gameObject.SetActive(true);
		achievementText.text = "EVENT UNLOCKED \n"  + funnyEvent.Description;
	}

	public void  UpdateHpText(){
		imgHp.fillAmount = (DataManager.instance.oPlayerData.HpCount/10f);
	}



	public void  UpdateComboText(){
		if (DataManager.instance.oPlayerData.ComboCount > 1) {
			//comboText.text = "COMBO x " + DataManager.instance.oPlayerData.ComboCount;
		} else {
			//comboText.text = "";
		}
	}

	void AddListenersToButtons(){
		// spoko, ale mozna tez na scenie, ale tak chyba nawet lepiej,
		//bo wiesz gdzie masz te przypisania a na scenie czasami zniknie referncja
		ButtonMenu_ReStart_Easy.onClick.RemoveAllListeners ();
		ButtonMenu_ReStart_Easy.onClick.AddListener (() => DataManager.instance.ReStartGame (DataManager.instance.oPlayerData.CurrentStage,DataManager.instance.easyMode,1));
		ButtonMenu_ReStart_Medium.onClick.RemoveAllListeners ();
		ButtonMenu_ReStart_Medium.onClick.AddListener (() => DataManager.instance.ReStartGame (DataManager.instance.oPlayerData.CurrentStage,DataManager.instance.mediumMode,2));
		ButtonMenu_ReStart_Hard.onClick.RemoveAllListeners ();
		ButtonMenu_ReStart_Hard.onClick.AddListener (() => DataManager.instance.ReStartGame (DataManager.instance.oPlayerData.CurrentStage,DataManager.instance.hardMode,3));




		ButtonQuick_Restart.onClick.RemoveAllListeners ();
		ButtonQuick_Restart.onClick.AddListener (() => DataManager.instance.ReStartGame (DataManager.instance.oPlayerData.CurrentStage,DataManager.instance.failPercentage,DataManager.instance.pointsMultipler));

//		ButtonGameWon_ReStart.onClick.RemoveAllListeners ();
//		ButtonGameWon_ReStart.onClick.AddListener (() => DataManager.instance.ReStartGame (DataManager.instance.oPlayerData.CurrentStage));
//
//		ButtonGameWon_ExitMenu.onClick.RemoveAllListeners ();
//		ButtonGameWon_ExitMenu.onClick.AddListener (() => DataManager.instance.BackToMenu ());

		ButtonPause_Resume.onClick.RemoveAllListeners ();
		ButtonPause_Resume.onClick.AddListener (() => DataManager.instance.ResumeGame ());

		ButtonPause_Pause.onClick.RemoveAllListeners ();
		ButtonPause_Pause.onClick.AddListener (() => DataManager.instance.PauseGame ());

		ButtonPause_RestartGame.onClick.RemoveAllListeners ();
		ButtonPause_RestartGame.onClick.AddListener (() => DataManager.instance.ReStartGame (DataManager.instance.oPlayerData.CurrentStage,DataManager.instance.failPercentage,DataManager.instance.pointsMultipler));

		ButtonPause_BackToMenu.onClick.RemoveAllListeners ();
		ButtonPause_BackToMenu.onClick.AddListener (() => DataManager.instance.BackToMenu ());

//		ButtonMenu_RandomEvent.onClick.RemoveAllListeners ();
//		ButtonMenu_RandomEvent.onClick.AddListener (() => FunnyEventsManager.instance.RandomizeEventNumber ());

		ButtonMenu_Exit.onClick.RemoveAllListeners ();
		ButtonMenu_Exit.onClick.AddListener (() => DataManager.instance.ExitGame());

		ButtonPause_BackToMenuAfterLost.onClick.RemoveAllListeners ();
		ButtonPause_BackToMenuAfterLost.onClick.AddListener (() => DataManager.instance.BackToMenu ());

		ButtonMenu_SoundOff.onClick.RemoveAllListeners ();
		ButtonMenu_SoundOff.onClick.AddListener (() => SoundsManager.Instance.MuteAll());

		ButtonMenu_MusicOff.onClick.RemoveAllListeners ();
		ButtonMenu_MusicOff.onClick.AddListener (() => MusicManager.instance.MuteMusic());

		ButtonStopSpawnInGame.onClick.RemoveAllListeners ();
		ButtonStopSpawnInGame.onClick.AddListener (() => SpawnerHouses.instance.StopSpawnHouses());
	}
	public void  UIChangeGameState (GameState gameState)
	{
		PanelMenu.SetActive (false);
		PanelPause.SetActive (false);
		ButtonQuick_Restart.gameObject.SetActive (false);
	    Panel_GameLost.SetActive (false);
		ButtonPause_Pause.enabled = false;
		ButtonStopSpawnInGame.gameObject.SetActive (false);
		//DataManager.instance.oGameState = gameState;
		switch (gameState) {

		case GameState.GameOn:
			ButtonPause_Pause.enabled = true;

			managerOfGlobEvents.TriggerDataChange ();
			ButtonMenu_RandomEvent.transform.localScale = Vector3.zero;
			ButtonMenu_ReStart_Medium.transform.localScale = Vector3.zero;
			ButtonMenu_WatchAdd.transform.localScale = Vector3.zero;
			//Menu_Title.transform.localScale = Vector3.zero;
			ButtonMenu_MusicOff.transform.localScale = Vector3.zero;
			ButtonMenu_SoundOff.transform.localScale = Vector3.zero;
			ButtonStopSpawnInGame.gameObject.SetActive (true);
			//ButtonQuick_Restart.transform.localScale = Vector3.zero;
			//ButtonPause_BackToMenuAfterLost.transform.localScale = Vector3.zero;
			//GameOverTextAsImage.transform.localScale = Vector3.zero;
			//.transform.localScale = Vector3.zero;
			break;
		case GameState.Menu:
			StopCoroutine("finishGame" );
			managerOfGlobEvents.TriggerDataChange ();
			PanelMenu.SetActive (true);
			Sprite newSprite =(Resources.Load<Sprite> ((PREFAB_PATH +"Sprites/Backgrounds/"  + "Logo")));
			GameBackground.GetComponent<SpriteRenderer> ().sprite = newSprite;
			UImanager.instance.StopCoroutine("finishGame" );
			break;
		case GameState.GamePause:
			PanelPause.SetActive (true);
			break;
		case GameState.GameLost:
		//	GameOver_prefab.SetActive (true);
			Panel_GameLost.SetActive (true);
			StartCoroutine (finishGame_Lost ());
			break;
//		case GameState.GameWon:
//			//	GameOver_prefab.SetActive (true);
//			Panel_GameWon.SetActive (true);
//			StartCoroutine (finishGame_Won ());
//			break;
		}
	}
	public IEnumerator finishGame_Lost(){
		//GameOver_prefab.SetActive (true);
	
		ButtonQuick_Restart.gameObject.SetActive (true);
		//GameOver_prefab.transform.position = GameObject.Find ("PlaceHolder_GameOver").transform.position;

		yield return new WaitForSeconds (5f);
		if (DataManager.instance.oGameState!=GameState.Menu) {
			managerOfGlobEvents.TriggerOnChangeGameState (GameState.Menu);
		}
	}


	public IEnumerator finishGame_Won(){
		//GameOver_prefab.SetActive (true);
		ButtonQuick_Restart.gameObject.SetActive (true);
		//GameOver_prefab.transform.position = GameObject.Find ("PlaceHolder_GameOver").transform.position;

		yield return new WaitForSeconds (5f);
		if (DataManager.instance.oGameState!=GameState.Menu) {
			managerOfGlobEvents.TriggerOnChangeGameState (GameState.Menu);
		}
	}


	//these two are about buying events:
	void ShowTextInComicCloud(string textToShow){

//		PanelBackground_Animator.SetTrigger("UnsuccesfulBought");
//		Img_RandomEvent_Animator.SetTrigger("UnsuccesfulBought");
		SoundsManager.Instance.PlaySound (SoundType.NotEnoughMoney);
	}

	public void  UpdateEventDescript(){

		FunnyEventDescriptionText.text = DataManager.instance.oPlayerData.CurrentFunnyEvent.Description;
	//	Img_RandomEvent_Animator.SetTrigger("SuccesfulBought");
		changeEventImage (DataManager.instance.oPlayerData.CurrentFunnyEvent.Number.ToString ());
	}

	void changeEventImage(string eventImageName){

		Sprite newSprite =(Resources.Load<Sprite> (PREFAB_PATH + "UI_prefabs/EventsImages/" + eventImageName));
		ImageEvent_Visualization.sprite = newSprite;
	}
}

