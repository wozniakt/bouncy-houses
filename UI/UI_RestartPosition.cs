﻿using UnityEngine;
using System.Collections;

public class UI_RestartPosition : MonoBehaviour
{
	Vector2 posStart;
	// Use this for initialization
	void OnEnable ()
	{
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0,0);
		posStart = transform.position;
	}
	
	// Update is called once per frame
	void OnDisable ()
	{
		transform.position = posStart;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0,0);
	}
}

