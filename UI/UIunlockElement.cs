﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIunlockElement : MonoBehaviour
{
	public int HighscoreToUnlock;

		// Use this for initialization
	void OnEnable ()
	{
		ManagerOfGlobalEvents.instance.eDataChange += this.UpdateThis;
		ManagerOfGlobalEvents.instance.TriggerDataChange();
	}
	void OnDisable ()
	{
		ManagerOfGlobalEvents.instance.eDataChange -= this.UpdateThis;
	}

	void UpdateThis(){
		
		if ( DataManager.instance.oPlayerData.HighscorePointsCount>=HighscoreToUnlock) {
			
			GetComponent<Image> ().color =Color. white;
			GetComponentInChildren<Text> ().text = "" ;
		} else {
			GetComponent<Image> ().color =Color.black;
			GetComponentInChildren<Text> ().text = "Unlock at \n" + HighscoreToUnlock + "\n points";
			GetComponentInChildren<Text> ().fontSize = 12;
			if (GetComponent<Button> ()) {
				GetComponent<Button> ().enabled = false;
			}

		}
	}
	
	// Update is called once per frame

}

